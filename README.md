# Board Game

The purpose of this project is to apply the knowledge acquired with the JAVA language by implementing a chess game.

## Features

- [x] Rules of piece movements
- [x] Rules of piece capture
- [x] Special moves: en passant
- [x] Special moves: castling
- [x] Vitory condition: check mate

## Roadmap

New features or pendency for implement in the game:

- [ ] Choice piece in Pawn promotion
- [ ] Vitory condition: game draw


