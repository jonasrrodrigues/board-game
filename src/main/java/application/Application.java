package application;

import application.interfaces.ConsoleColors;
import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;
import chessgame.entities.exeptions.ChessGameServiceException;
import chessgame.entities.pieces.ChessPiece;
import chessgame.services.ChessGameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static application.interfaces.ConsoleColors.*;
import static application.interfaces.UI.*;
import static board.entities.enums.PieceColor.WITHE;

public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChessGameService.class);

    public static void main(String[] args) {
        System.out.print(BLACK_BACKGROUND_BRIGHT + WHITE_BOLD_BRIGHT + "Welcome to chess game" + RESET + "\n");
        System.out.print(WHITE_UNDERLINED + "This game is terminal-based like old times" + "\n\n\n" + RESET);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                startGame();
            }
        }, 3000L); // 30

    }

    public static void startGame() {
        String error = null;
        ChessGameService service = new ChessGameService();

        while (!service.isCheckmate()) {
            clear();
            if (service.isKingInCheck()) {
                error = service.getTurn() + " King in check!";
            }
            System.out.print(printBoardState(service.getBoard()));
            try {
                ChessPosition currentPosition;
                ChessPosition toPosition;
                Scanner scanner = new Scanner(System.in);
                final String turnOf = service.getTurn() == WITHE ? YELLOW_BACKGROUND + WHITE_BOLD_BRIGHT + service.getTurn() + RESET :
                        BLACK_BACKGROUND_BRIGHT + WHITE_BOLD_BRIGHT + service.getTurn().toString() + RESET;

                System.out.println();
                System.out.println("Turn of " + turnOf);
                System.out.println("White captured: " + service.getCapturedOf(PieceColor.WITHE).toString());
                System.out.println("Black captured: " + service.getCapturedOf(PieceColor.BLACK).toString());
                if (error != null) {
                    System.out.println(ConsoleColors.RED + error);
                    error = null;
                }
                System.out.print(WHITE_BOLD_BRIGHT);
                System.out.print("Which piece do you want move? Insert a chess position: ");
                currentPosition = ChessPosition.valueOf(scanner.next().toUpperCase(Locale.ROOT));

                if (service.isValidPieceForMove(currentPosition)) {
                    ChessPiece piece = service.getChessPiece(currentPosition);
                    Set<ChessPosition> possibleMoves = service.possibleMovesInTheBoard(piece, currentPosition);

                    if (possibleMoves.size() == 0) {
                        error = String.format("Has no possible moves for piece %s in %s", piece, currentPosition);
                    } else {
                        System.out.print(printPossibleMoves(service.getBoard(), service.possibleMovesInTheBoard(piece, currentPosition)));
                        System.out.println();
                        System.out.print("Move " + WHITE_BOLD_BRIGHT + WHITE_UNDERLINED + piece + RESET + WHITE_BOLD_BRIGHT + " to which position? ");
                        toPosition = ChessPosition.valueOf(scanner.next().toUpperCase(Locale.ROOT));
                        service.movePiece(currentPosition, toPosition);
                    }
                } else {
                    error = "This is invalid piece to move or has no possible moves!";
                }

            } catch (ChessGameServiceException e) {
                error = e.getMessage();
            } catch (IllegalArgumentException e) {
                error = "The position is invalid. Valid position e.g: A1, A2, H5..";
            }
        }
        System.out.println(printBoardState(service.getBoard()));
        System.out.println(String.format("CHECKMATE! %s WINS", service.getTurn() == PieceColor.WITHE ? PieceColor.BLACK : WITHE));
    }
}
