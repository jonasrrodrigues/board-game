package application.interfaces;

import board.entities.Position;
import board.entities.enums.PieceColor;
import chessgame.entities.board.ChessBoard;
import chessgame.entities.enums.ChessPosition;
import chessgame.entities.pieces.ChessPiece;

import java.util.HashSet;
import java.util.Set;

import static application.interfaces.ConsoleColors.*;
import static board.entities.enums.PieceColor.BLACK;
import static board.entities.enums.PieceColor.WITHE;
import static chessgame.entities.enums.ChessPosition.toChessPosition;

public class UI {

    private static final String SPACE = " ";
    private static final String NEW_LINE = "\n";

    public static String printPossibleMoves(ChessBoard board, Set<ChessPosition> positions) {
        return printBoardState(board, positions);
    }

    public static String printBoardState(ChessBoard board) {
        return printBoardState(board, new HashSet<>());
    }

    private static String printBoardState(ChessBoard board, Set<ChessPosition> positions) {

        StringBuilder paint = new StringBuilder();
        PieceColor currentColor = BLACK;

        for (int row = 0; row < board.getRows(); row++) {
            paint.append(NEW_LINE);
            paint.append(WHITE_BOLD_BRIGHT)
                    .append(8 - row)
                    .append(SPACE);
            currentColor = currentColor == BLACK ? WITHE : BLACK;
            for (int col = 0; col < board.getColumns(); col++) {
                Position position = new Position(7 - row, col);
                ChessPiece piece = board.getChessPiece(toChessPosition(position));
                if (positions.contains(toChessPosition(position))) {
                    paint.append(squareBoards(piece, currentColor, true));
                } else {
                    paint.append(squareBoards(piece, currentColor, false));
                }
                currentColor = currentColor == BLACK ? WITHE : BLACK;
            }
        }

        paint.append(NEW_LINE).append(SPACE);

        for (char alphabet = 'A'; alphabet <= 'H'; alphabet++) {
            paint.append(SPACE)
                    .append(SPACE)
                    .append(WHITE_BOLD_BRIGHT)
                    .append(alphabet);
        }
        return paint.toString();
    }

    private static String squareBoards(ChessPiece piece, PieceColor color, boolean movements) {
        String squareColor;
        if (movements) {
            squareColor = BLACK == color ? BLUE_BACKGROUND : BLUE_BACKGROUND_BRIGHT;
        } else {
            squareColor = BLACK == color ? BLACK_BACKGROUND_BRIGHT : WHITE_BACKGROUND_BRIGHT;
        }
        if (piece == null) {
            return squareColor + "   " + RESET;
        }
        final String pieceColor = piece.getColor() == BLACK ? BLACK_BOLD : YELLOW_BOLD;
        return squareColor + pieceColor + " " + piece + " " + RESET;
    }

    public static void clear() {
        for (int i = 0; i < 500; i++) {
            System.out.println();
        }
    }
}
