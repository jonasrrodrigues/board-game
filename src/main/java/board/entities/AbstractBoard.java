package board.entities;

import board.entities.exceptions.BoardException;

public abstract class AbstractBoard implements Board {

    private int rows;
    private int columns;
    private Piece[][] pieces;

    public AbstractBoard(int rows, int cols) throws BoardException {
        if (rows <= 0 || cols <= 0) {
            throw new BoardException("The row and column must be greater than 0.");
        }
        this.rows = rows;
        this.columns = cols;
        this.pieces = new Piece[rows][cols];
    }

    @Override
    public int getRows() {
        return rows;
    }

    @Override
    public int getColumns() {
        return columns;
    }

    @Override
    public Piece getPiece(Position position) {
        if (position == null) {
            return null;
        }
        return pieces[position.getRow()][position.getColumn()];
    }

    @Override
    public Piece[][] getPieces() {
        return this.pieces;
    }

    @Override
    public void placePiece(Piece piece, Position position) throws BoardException {
        if (!isValidPosition(position)) {
            throw new BoardException(String.format("This is invalid position (%s,%s) for move or place a piece.", position.getRow(), position.getColumn()));
        }
        this.pieces[position.getRow()][position.getColumn()] = piece;
    }

    public boolean isValidPosition(Position position) {
        if (position == null) {
            return false;
        }
        if (position.getRow() < 0 || position.getRow() > this.rows - 1) {
            return false;
        }
        if (position.getColumn() < 0 || position.getColumn() > this.columns - 1) {
            return false;
        }
        if (getPiece(position) != null) {
            return false;
        }
        return true;
    }

    @Override
    public void removePiece(Position position) {
        this.pieces[position.getRow()][position.getColumn()] = null;
    }

}
