package board.entities;

import board.entities.exceptions.BoardException;

interface Board {

    int getRows();

    int getColumns();

    Piece[][] getPieces();

    Piece getPiece(Position position);

    void placePiece(Piece piece, Position position) throws BoardException;

    void removePiece(Position position);

}
