package board.entities;

import board.entities.enums.PieceColor;

public interface Piece {
    PieceColor getColor();

    boolean isFirstMove();
}
