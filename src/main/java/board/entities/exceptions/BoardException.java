package board.entities.exceptions;

public class BoardException extends Exception {
    public BoardException(String message) {
        super(message);
    }
}
