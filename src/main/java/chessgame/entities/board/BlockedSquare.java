package chessgame.entities.board;

import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;
import chessgame.entities.pieces.ChessPiece;

import java.util.Set;

public class BlockedSquare extends ChessPiece {

    public BlockedSquare(PieceColor color) {
        super(color);
    }

    @Override
    public Set<ChessPosition> possibleMoves(ChessPosition startPosition) {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof BlockedSquare) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "*";
    }
}
