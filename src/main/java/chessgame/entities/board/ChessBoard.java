package chessgame.entities.board;

import board.entities.AbstractBoard;
import board.entities.Position;
import board.entities.exceptions.BoardException;
import chessgame.entities.enums.ChessPosition;
import chessgame.entities.pieces.ChessPiece;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class ChessBoard extends AbstractBoard {

    public ChessBoard() throws BoardException{
        super(8, 8);
    }

    public ChessPiece getChessPiece(ChessPosition position) {
        if (position == null) {
            return null;
        }
        return (ChessPiece) getPiece(new Position(position.row,position.col));
    }

    public void placePiece(ChessPiece piece, ChessPosition position) throws BoardException {
        placePiece(piece, new Position(position.row, position.col));
    }

    public void removePiece(ChessPosition position) {
        removePiece(new Position(position.row, position.col));
    }


    public boolean havePiece(ChessPosition position) {
        return getChessPiece(position) != null;
    }

    public Map<ChessPiece, ChessPosition> getAllPieces() {
        return Arrays.stream(ChessPosition.values())
                .filter(this::havePiece)
                .collect(Collectors.toMap(this::getChessPiece, position -> position));
    }
}
