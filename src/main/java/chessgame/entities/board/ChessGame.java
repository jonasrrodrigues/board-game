package chessgame.entities.board;

import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;
import chessgame.entities.pieces.ChessPiece;

import java.util.ArrayList;
import java.util.List;

import static board.entities.enums.PieceColor.BLACK;
import static board.entities.enums.PieceColor.WITHE;

public class ChessGame {

    private int round;

    private PieceColor turnOf;

    private boolean isCheckmate;

    private List<ChessPiece> witheDeads = new ArrayList<>();

    private List<ChessPiece> blackDeads = new ArrayList<>();

    private ChessPosition enPassant;
    private int enPassantRound;

    public ChessGame() {
        this.round = 0;
        this.turnOf = WITHE;
        this.isCheckmate = false;
    }

    public int getEnPassantRound() {
        return enPassantRound;
    }

    public void setEnPassantRound(int enPassantRound) {
        this.enPassantRound = enPassantRound;
    }

    public ChessPosition getEnPassant() {
        return enPassant;
    }

    public void setEnPassant(ChessPosition enPassant) {
        this.enPassant = enPassant;
    }

    public int getRound() {
        return round;
    }

    public PieceColor getTurnOf() {
        return turnOf;
    }

    public List<ChessPiece> getWitheDeads() {
        return witheDeads;
    }

    public List<ChessPiece> getBlackDeads() {
        return blackDeads;
    }

    public boolean isCheckmate() {
        return isCheckmate;
    }

    public void nextRound() {
        this.round++;
        nextTurn();
    }

    private void nextTurn() {
        this.turnOf = this.turnOf == WITHE ? BLACK : WITHE;
    }

    public void capturePiece(ChessPiece piece) {
        if (piece.getColor() == BLACK) {
            blackDeads.add(piece);
        } else {
            witheDeads.add(piece);
        }
    }

    public void setCheckmate() {
        isCheckmate = true;
    }
}
