package chessgame.entities.enums;

public enum MovementDirections {

    HORIZONTAL,
    VERTICAL,
    DIAGONAL,
    FORWARD,
    DIAGONAL_FORWARD,
    KNIGHT_MOVEMENT;

}
