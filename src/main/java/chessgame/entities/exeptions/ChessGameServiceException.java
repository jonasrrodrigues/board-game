package chessgame.entities.exeptions;

public class ChessGameServiceException extends Exception {
    public ChessGameServiceException(String message) {
        super(message);
    }
}
