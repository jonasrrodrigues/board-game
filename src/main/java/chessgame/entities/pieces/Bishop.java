package chessgame.entities.pieces;

import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;

import java.util.Set;

import static chessgame.services.ChessPieceMovements.diagonalMovements;

public class Bishop extends ChessPiece {

    private static final int MAX_MOVES = 7;

    public Bishop(PieceColor color) {
        super(color);
    }

    @Override
    public String toString() {
        return "B";
    }

    @Override
    public Set<ChessPosition> possibleMoves(ChessPosition position) {
        return diagonalMovements(position, MAX_MOVES);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Bishop) {
            return true;
        }
        return false;
    }
}
