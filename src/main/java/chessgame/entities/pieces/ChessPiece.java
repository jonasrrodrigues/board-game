package chessgame.entities.pieces;

import board.entities.Piece;
import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;

import java.util.Set;

public abstract class ChessPiece implements Piece {

    private PieceColor color;

    private boolean isFirstMove = true;

    protected ChessPiece(PieceColor color) {
        this.color = color;
    }

    public void setFirstMove() {
        isFirstMove = false;
    }
    public abstract Set<ChessPosition> possibleMoves(ChessPosition startPosition);
    @Override
    public boolean isFirstMove(){
        return isFirstMove;
    }

    @Override
    public PieceColor getColor() {
        return color;
    }

}
