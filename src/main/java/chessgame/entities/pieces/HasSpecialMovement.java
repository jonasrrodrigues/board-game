package chessgame.entities.pieces;

public interface HasSpecialMovement {
    default boolean hasSpecialMovement() {
        return true;
    };
}
