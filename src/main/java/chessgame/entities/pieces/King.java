package chessgame.entities.pieces;

import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static chessgame.services.ChessPieceMovements.*;

public class King extends ChessPiece implements HasSpecialMovement {

    private static final int MAX_MOVES = 1;

    public King(PieceColor color) {
        super(color);
    }

    @Override
    public Set<ChessPosition> possibleMoves(ChessPosition position) {
        Set<ChessPosition> movePositions = new HashSet<>();
        movePositions.addAll(verticalMovements(position, MAX_MOVES));
        movePositions.addAll(horizontalMovements(position, MAX_MOVES));
        movePositions.addAll(diagonalMovements(position, MAX_MOVES));
        return movePositions;
    }
    @Override
    public String toString() {
        return "K";
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof King) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getClass(), this.getColor());
    }


}
