package chessgame.entities.pieces;

import board.entities.Position;
import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static chessgame.entities.enums.ChessPosition.toChessPosition;

public class Knight extends ChessPiece {

    public Knight(PieceColor color) {
        super(color);
    }

    @Override
    public String toString() {
        return "N";
    }

    @Override
    public Set<ChessPosition> possibleMoves(ChessPosition startPosition) {
        List<ChessPosition> movements = new ArrayList<>();

        movements.add(toChessPosition(new Position(startPosition.row + 1, startPosition.col + 2)));
        movements.add(toChessPosition(new Position(startPosition.row + 1, startPosition.col - 2)));
        movements.add(toChessPosition(new Position(startPosition.row - 1, startPosition.col - 2)));
        movements.add(toChessPosition(new Position(startPosition.row - 1, startPosition.col + 2)));

        movements.add(toChessPosition(new Position(startPosition.row + 2, startPosition.col + 1)));
        movements.add(toChessPosition(new Position(startPosition.row + 2, startPosition.col - 1)));
        movements.add(toChessPosition(new Position(startPosition.row - 2, startPosition.col + 1)));
        movements.add(toChessPosition(new Position(startPosition.row - 2, startPosition.col - 1)));

        return movements.stream()
                .filter(move -> move != null)
                .collect(Collectors.toSet());
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Knight) {
            return true;
        }
        return false;
    }
}
