package chessgame.entities.pieces;

import board.entities.Position;
import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static chessgame.entities.enums.ChessPosition.toChessPosition;

public class Pawn extends ChessPiece implements HasSpecialMovement {

    public Pawn(PieceColor color) {
        super(color);
    }

    @Override
    public Set<ChessPosition> possibleMoves(ChessPosition currentPosition) {

        List<ChessPosition> movePositions = new ArrayList<>();
        int moveForward = -1; // moves black pawn

        if (this.getColor() == PieceColor.WITHE) {
            moveForward = 1;
        }

        if (isFirstMove()) {
            movePositions.add(toChessPosition(new Position(currentPosition.row + moveForward * 2, currentPosition.col)));
        }
        movePositions.add(toChessPosition(new Position(currentPosition.row + moveForward, currentPosition.col)));

        return movePositions.stream().filter(p -> p != null).collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return "P";
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Pawn) {
            return true;
        }
        return false;
    }

}
