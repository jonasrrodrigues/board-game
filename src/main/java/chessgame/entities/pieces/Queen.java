package chessgame.entities.pieces;

import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;

import java.util.HashSet;
import java.util.Set;

import static chessgame.services.ChessPieceMovements.*;

public class Queen extends ChessPiece {

    private static final int MAX_MOVES = 7;

    public Queen(PieceColor color) {
        super(color);
    }

    @Override
    public String toString() {
        return "Q";
    }

    @Override
    public Set<ChessPosition> possibleMoves(ChessPosition startPosition) {
        Set<ChessPosition> movements = new HashSet<>();
        movements.addAll(diagonalMovements(startPosition, MAX_MOVES));
        movements.addAll(horizontalMovements(startPosition, MAX_MOVES));
        movements.addAll(verticalMovements(startPosition, MAX_MOVES));
        return movements;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Queen) {
            return true;
        }
        return false;
    }
}
