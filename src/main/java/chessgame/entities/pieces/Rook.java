package chessgame.entities.pieces;

import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;

import java.util.HashSet;
import java.util.Set;

import static chessgame.services.ChessPieceMovements.horizontalMovements;
import static chessgame.services.ChessPieceMovements.verticalMovements;

public class Rook extends ChessPiece implements HasSpecialMovement {

    private static final int MAX_MOVES = 7;

    public Rook(PieceColor color) {
        super(color);
    }

    @Override
    public String toString() {
        return "R";
    }

    @Override
    public Set<ChessPosition> possibleMoves(ChessPosition startPosition) {
        Set<ChessPosition> movePositions = new HashSet<>();
        movePositions.addAll(horizontalMovements(startPosition, MAX_MOVES));
        movePositions.addAll(verticalMovements(startPosition, MAX_MOVES));
        return movePositions;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Rook) {
            return true;
        }
        return false;
    }

}
