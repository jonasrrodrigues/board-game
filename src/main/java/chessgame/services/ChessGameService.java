package chessgame.services;

import board.entities.Position;
import board.entities.enums.PieceColor;
import board.entities.exceptions.BoardException;
import chessgame.entities.board.BlockedSquare;
import chessgame.entities.board.ChessBoard;
import chessgame.entities.board.ChessGame;
import chessgame.entities.enums.ChessPosition;
import chessgame.entities.exeptions.ChessGameServiceException;
import chessgame.entities.pieces.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static board.entities.enums.PieceColor.BLACK;
import static board.entities.enums.PieceColor.WITHE;
import static chessgame.entities.enums.ChessPosition.*;
import static chessgame.services.ChessPieceMovements.*;

public class ChessGameService {

    private static final Set<ChessPosition> ROOK_POSITIONS = new HashSet<>(Arrays.asList(A1,H1,A8,H8));
    private static final Set<ChessPosition> CASTLING_POSITIONS = new HashSet<>(Arrays.asList(C1,G1,C8,G8));
    private static final Logger LOGGER = LoggerFactory.getLogger(ChessGameService.class);

    private ChessBoard chessBoard;
    private ChessGame chessGame;

    public ChessGameService() {
        try {
            this.chessBoard = new ChessBoard();
            this.chessGame = new ChessGame();
        } catch (BoardException e) {
            LOGGER.error("Error on start chess game: " + e);
        }
        startGame();
    }

    public ChessGameService(ChessBoard board, ChessGame game) {
        this.chessBoard = board;
        this.chessGame = game;
    }

    public void startGame() {
        placeChessPieces(chessBoard);
    }

    public ChessBoard getBoard() {
        return chessBoard;
    }

    public PieceColor getTurn() {
        return chessGame.getTurnOf();
    }

    public int getRound() {
        return chessGame.getRound();
    }

    public ChessPiece getChessPiece(ChessPosition position) {
        return chessBoard.getChessPiece(position);
    }

    public List<ChessPiece> getCapturedOf(PieceColor color) {
        return color == WITHE ? chessGame.getWitheDeads() : chessGame.getBlackDeads();
    }

    public void movePiece(ChessPosition currentPosition, ChessPosition toPosition) throws ChessGameServiceException {
        ChessPiece yourPiece = chessBoard.getChessPiece(currentPosition);
        Map<ChessPiece, ChessPosition> oldPositions = new HashMap<>();

        if (yourPiece == null) {
            throw new ChessGameServiceException("No have a piece on current position " + currentPosition);
        }

        if (yourPiece.getColor() != chessGame.getTurnOf()) {
            throw new ChessGameServiceException("Unable to move adversary piece");
        }

        Set<ChessPosition> possibleMoves = possibleMovesInTheBoard(yourPiece, currentPosition);

        if (possibleMoves.size() == 0) {
            throw new ChessGameServiceException("Has no possible moves for this piece!");
        }

        if (possibleMoves.contains(toPosition)) {
            try {
                ChessPiece adversaryPiece = chessBoard.getChessPiece(toPosition);

                if (yourPiece instanceof King && CASTLING_POSITIONS.contains(toPosition)) {
                    moveToCastling(currentPosition, toPosition, yourPiece);
                    nextRound();
                    return;
                }

                if (adversaryPiece != null) {
                    oldPositions.put(adversaryPiece, toPosition);
                    capturePiece(yourPiece, toPosition);
                }

                if (yourPiece instanceof Pawn) {
                    if (promotionPawn(toPosition)) {
                        yourPiece =  new Queen(getTurn());
                    }
                    int nrMoves = Math.abs(currentPosition.row - toPosition.row);
                    if ( nrMoves == 2 ) {
                       setEnPassantSquare(toPosition, getRound());
                    } else if (chessGame.getEnPassant() == toPosition) {
                        oldPositions.putAll(captureEnPassant(toPosition));
                    }
                }

                oldPositions.put(yourPiece, currentPosition);
                chessBoard.removePiece(currentPosition);
                chessBoard.placePiece(yourPiece, toPosition);

                if (isKingInCheck()) {
                    chessBoard.removePiece(toPosition);
                    rollbackPositions(oldPositions);
                    throw new ChessGameServiceException("Can't move to a position that leaves the king in check.");
                }
                yourPiece.setFirstMove();
                if (getRound() >= chessGame.getEnPassantRound()) {
                    chessGame.setEnPassant(null);
                }
                nextRound();

            } catch (BoardException e) {
                chessBoard.removePiece(toPosition);
                rollbackPositions(oldPositions);
                LOGGER.error("Error in move piece: " + e);
            }

        } else {
            chessBoard.removePiece(toPosition);
            rollbackPositions(oldPositions);
            throw new ChessGameServiceException("Don't possible place the piece " + yourPiece + " in position " + toPosition);
        }
    }

    private Map<ChessPiece, ChessPosition> captureEnPassant(ChessPosition toPosition)  {
        final int direction = getTurn() == BLACK ? 1 : -1;
        Map<ChessPiece, ChessPosition> oldPositions = new HashMap<>();
        ChessPosition capturePosition = toChessPosition(new Position(toPosition.row + direction, toPosition.col));
        ChessPiece adversaryPiece = getChessPiece(capturePosition);
        oldPositions.put(getChessPiece(capturePosition), capturePosition);
        chessBoard.removePiece(capturePosition);
        chessGame.capturePiece(adversaryPiece);
        return oldPositions;
    }

    private void rollbackPositions(Map<ChessPiece, ChessPosition> oldPositions) {
        oldPositions.forEach( (piece, position) -> {
            try {
                chessBoard.placePiece(piece, position);
            } catch (BoardException e) {
                LOGGER.error("Error in rollback: " + e);
            }
        });
    }

    private void moveToCastling(ChessPosition currentPosition, ChessPosition toPosition, ChessPiece piece) {
        try {
            ChessPosition rookPosition = getRookPositionBeforeCastling(toPosition);
            ChessPiece rook = getChessPiece(rookPosition);
            chessBoard.removePiece(currentPosition);
            chessBoard.removePiece(rookPosition);
            chessBoard.placePiece(piece, toPosition);
            chessBoard.placePiece(rook, getRookPositionAfterCastling(toPosition));
            piece.setFirstMove();
            rook.setFirstMove();
        } catch (BoardException e) {
            LOGGER.error("Error in castling: " + e);
        }

    }

    private void setEnPassantSquare(ChessPosition toPosition, int round) {
        int direction = getTurn() == BLACK ? 1 : -1;
        ChessPosition enPassantPosition = toChessPosition(new Position(toPosition.row + direction, toPosition.col));
        chessGame.setEnPassant(enPassantPosition);
        chessGame.setEnPassantRound(round + 1);
    }

    private Set<ChessPosition> getAllSquaresAttacked(Map<ChessPiece, ChessPosition> allPieces, ChessPosition kingPosition) {
        Set<ChessPosition> attackedSquares = new HashSet<>();
        PieceColor color = getTurn() == WITHE ? BLACK : WITHE;
        allPieces.forEach((piece, position) -> {
            if (piece.getColor() == color) {
                Set<ChessPosition> possibleMoves = possibleMovesInTheBoard(piece, position, false);
                final boolean filter = kingPosition != null ? possibleMoves.contains(kingPosition) : true;
                if (filter) {
                        attackedSquares.addAll(possibleMoves);
                    }
                }
        });
        return attackedSquares;
    }

    private Set<ChessPosition> getAllSquaresDefended(Map<ChessPiece, ChessPosition> allPieces, ChessPiece king) {
        Set<ChessPosition> squaresDefended = new HashSet<>();
        allPieces.forEach( (piece, position) -> {
            if ( piece.getColor() == getTurn() && !piece.equals(king) && !(piece instanceof BlockedSquare)) {
                squaresDefended.addAll(possibleMovesInTheBoard(piece, position));
            }
        });
        return squaresDefended;
    }

    public Set<ChessPosition> possibleMovesInTheBoard(ChessPiece piece, ChessPosition piecePosition, boolean acceptCastling) {

        if (piece == null || piecePosition == null) {
            return new HashSet<>();
        }

        if (piece instanceof Pawn) {
            return possibleMovesOfPawn(piece, piecePosition);
        }

        Set<ChessPosition> positions = piece.possibleMoves(piecePosition);
        Set<ChessPosition> forbidden = new HashSet<>();

        if (piece instanceof King && piece.isFirstMove() && acceptCastling) {
            positions.addAll(castlingMoves(piece, piecePosition));
        }

        positions.forEach(anotherPiecePosition -> {
            ChessPiece anotherPiece = getChessPiece(anotherPiecePosition);
            if (anotherPiece != null) {
                if (piecePosition.row > anotherPiecePosition.row) {
                    if (piecePosition.col > anotherPiecePosition.col) {
                        forbidden.addAll(diagonalLeftDownMovements(anotherPiecePosition, 7));
                    } else {
                        forbidden.addAll(diagonalRightDownMovements(anotherPiecePosition, 7));
                    }
                } else if (piecePosition.row < anotherPiecePosition.row) {
                    if (piecePosition.col > anotherPiecePosition.col) {
                        forbidden.addAll(diagonalLeftUpMovements(anotherPiecePosition, 7));
                    } else {
                        forbidden.addAll(diagonalRightUpMovements(anotherPiecePosition, 7));
                    }
                }

                if (piecePosition.row == anotherPiecePosition.row) {
                    if (piecePosition.col > anotherPiecePosition.col) {
                        forbidden.addAll(horizontalLeftMovements(anotherPiecePosition, 7));
                    } else {
                        forbidden.addAll(horizontalRightMovements(anotherPiecePosition, 7));
                    }
                }

                if (piecePosition.col == anotherPiecePosition.col) {
                    if (piecePosition.row > anotherPiecePosition.row) {
                        forbidden.addAll(verticalDownMovements(anotherPiecePosition, 7));
                    } else {
                        forbidden.addAll(verticalUpMovements(anotherPiecePosition, 7));
                    }
                }

                if (anotherPiece.getColor() == piece.getColor()) {
                    forbidden.add(anotherPiecePosition);
                }
            }
        });

        return positions.stream()
                .filter(position -> !forbidden.contains(position))
                .collect(Collectors.toSet());
    }

    public Set<ChessPosition> possibleMovesInTheBoard(ChessPiece piece, ChessPosition piecePosition) {
        return possibleMovesInTheBoard(piece, piecePosition, true);
    }
    public boolean isCheckmate() {
        return chessGame.isCheckmate();
    }

    boolean verifyCheckmate() throws BoardException {
        if (!isKingInCheck()) {
            return false;
        }
        ChessPiece king = new King(getTurn());
        ChessPosition kingPosition = chessBoard.getAllPieces().get(king);
        Set<ChessPosition> possibleKingMoves = possibleMovesInTheBoard(king, kingPosition);

        Map<ChessPiece, ChessPosition> allPieces = chessBoard.getAllPieces();
        Set<ChessPosition> attackedSquares = getAllSquaresAttacked(allPieces, kingPosition);
        Set<ChessPosition> defencedSquares = getAllSquaresDefended(allPieces, king);

        Set<ChessPosition> defencedSquaresWithAttack = defencedSquares.stream()
                .filter(pos -> getChessPiece(pos) != null)
                .collect(Collectors.toSet());
        defencedSquares.removeIf(position -> !attackedSquares.contains(position));
        defencedSquares.addAll(defencedSquaresWithAttack);

        if (attackedSquares.contains(kingPosition) && attackedSquares.containsAll(possibleKingMoves)) {
            ChessBoard chessBoardSimulation = new ChessBoard();
            ChessGameService simulatorService = new ChessGameService(chessBoardSimulation, this.chessGame);

            if (defencedSquares.size() == 0) {
                return true;
            }

            BiConsumer<ChessPiece, ChessPosition> placePiece = (piece, position) -> {
                try {
                    simulatorService.chessBoard.removePiece(position);
                    simulatorService.chessBoard.placePiece(piece, position);
                } catch (BoardException e) {
                    LOGGER.error(String.format(
                            "Error in the simulate checkmate (%s, %s): %s", piece, position, e.getMessage())
                    );
                }
            };
            allPieces.forEach((piece, position) -> {
                if (piece.getColor() == getTurn() && !piece.equals(king)) {
                    placePiece.accept(new BlockedSquare(getTurn()), position);
                } else {
                    placePiece.accept(piece, position);
                }
            });
            placePiece.accept(king, kingPosition);
            defencedSquares.forEach(pos -> placePiece.accept(new BlockedSquare(getTurn()), pos));
            return simulatorService.verifyCheckmate();
        } else {
            return false;
        }
    }

    public boolean isKingInCheck() {
        ChessPiece king = new King(getTurn());
        ChessPosition kingPosition = chessBoard.getAllPieces().get(king);
        Set<ChessPosition> possibleAttackSquare = getAllSquaresAttacked(chessBoard.getAllPieces(), kingPosition);
        return possibleAttackSquare.contains(kingPosition);
    }

    public boolean isValidPieceForMove(ChessPosition position) {
        ChessPiece piece = getChessPiece(position);

        if (piece == null || chessGame.isCheckmate()) {
            return false;
        }
        return piece != null && piece.getColor() == chessGame.getTurnOf();
    }

    private Set<ChessPosition> possibleMovesOfPawn(ChessPiece piece, ChessPosition piecePosition) {
        Set<ChessPosition> possibleMoves = piece.possibleMoves(piecePosition);
        Set<ChessPosition> forbidden = new HashSet<>();
        Set<ChessPosition> possibleCaptures = new HashSet<>();

        possibleMoves.forEach(
                position -> {
                    final ChessPiece anotherPiece = getChessPiece(position);
                    if (anotherPiece != null) {
                        forbidden.add(position);
                    }
                });

        if (piece.getColor() == WITHE) {
            possibleCaptures.addAll(diagonalLeftUpMovements(piecePosition, 1));
            possibleCaptures.addAll(diagonalRightUpMovements(piecePosition, 1));
        } else {
            possibleCaptures.addAll(diagonalLeftDownMovements(piecePosition, 1));
            possibleCaptures.addAll(diagonalRightDownMovements(piecePosition, 1));
        }

        if (possibleCaptures.contains(chessGame.getEnPassant())) {
            possibleMoves.add(chessGame.getEnPassant());
        }

        possibleMoves.addAll(
                possibleCaptures.stream()
                        .filter(pos -> {
                            final ChessPiece anotherPiece = getChessPiece(pos);
                            return anotherPiece != null && anotherPiece.getColor() != piece.getColor();
                        }).collect(Collectors.toList())
        );

        possibleMoves.removeIf( pos -> forbidden.contains(pos));

        return possibleMoves;
    }

    private Set<ChessPosition> castlingMoves(ChessPiece king, ChessPosition kingPosition) {
        Set<ChessPosition> possibleMoves = new HashSet<>();
        Set<ChessPosition> forbiddenPositions = getAllSquaresAttacked(chessBoard.getAllPieces(), null);

        BiConsumer<Set<ChessPosition>, Set<ChessPosition>> computeCastling = (castlingMoves, forbiddenMoves) -> {
            int blockedSquares = castlingMoves.stream()
                    .filter(forbiddenMoves::contains)
                    .collect(Collectors.toSet()).size();
            if(blockedSquares == 0) {
                possibleMoves.addAll(castlingMoves);
            }
        };

        ROOK_POSITIONS.forEach( position -> {
            final ChessPiece rook = getChessPiece(position);
            if (rook != null && rook.getColor() == king.getColor() && rook.isFirstMove()) {
                if (position.equals(A1) || position.equals(A8)) {
                    final Set<ChessPosition> castlingMoves = horizontalLeftMovements(kingPosition, 2);
                    computeCastling.accept(castlingMoves, forbiddenPositions);
                } else {
                    final Set<ChessPosition> castlingMoves =  horizontalRightMovements(kingPosition, 2);
                    computeCastling.accept(castlingMoves, forbiddenPositions);
                }
            }
        });
        return possibleMoves;
    }

    private boolean promotionPawn(ChessPosition position) {
        int lineOfPromove = getTurn() == WITHE ? 7 : 0;
        if (position.row == lineOfPromove) {
            return true;
        }
        return false;
    }

    private void capturePiece(ChessPiece piece, ChessPosition toPosition) throws ChessGameServiceException {
        ChessPiece adversaryPiece = chessBoard.getChessPiece(toPosition);
        if (adversaryPiece.getColor() == piece.getColor()) {
            throw new ChessGameServiceException("Unable to capture your piece!");
        }
        chessGame.capturePiece(adversaryPiece);
        chessBoard.removePiece(toPosition);
    }

    private void placeChessPieces(ChessBoard board) {
        try {
            for (int i = 0; i < chessBoard.getColumns(); i++) {
                board.placePiece(new Pawn(WITHE), toChessPosition(new Position(1, i)));
            }
            for (int i = 0; i < chessBoard.getColumns(); i++) {
                board.placePiece(new Pawn(PieceColor.BLACK), toChessPosition(new Position(6, i)));
            }

            board.placePiece(new Bishop(WITHE), C1);
            board.placePiece(new Bishop(WITHE), F1);
            board.placePiece(new Bishop(PieceColor.BLACK), C8);
            board.placePiece(new Bishop(PieceColor.BLACK), F8);

            board.placePiece(new Knight(WITHE), B1);
            board.placePiece(new Knight(WITHE), G1);
            board.placePiece(new Knight(PieceColor.BLACK), B8);
            board.placePiece(new Knight(PieceColor.BLACK), G8);

            board.placePiece(new Rook(WITHE), A1);
            board.placePiece(new Rook(WITHE), H1);
            board.placePiece(new Rook(PieceColor.BLACK), A8);
            board.placePiece(new Rook(PieceColor.BLACK), H8);

            board.placePiece(new Queen(WITHE), D1);
            board.placePiece(new Queen(PieceColor.BLACK), D8);

            board.placePiece(new King(WITHE), E1);
            board.placePiece(new King(PieceColor.BLACK), E8);

        } catch (BoardException e) {
            LOGGER.error("Place error: " + e);
        }
    }

    private void nextRound() throws BoardException {
        chessGame.nextRound();
        if (verifyCheckmate()) {
            chessGame.setCheckmate();
        }
    }

    private ChessPosition getRookPositionAfterCastling(ChessPosition initial) {
        switch (initial) {
            case C1: return D1;
            case G1: return F1;
            case C8: return D8;
            case G8: return F8;
            default: return initial;
        }
    }

    private ChessPosition getRookPositionBeforeCastling(ChessPosition initial) {
        switch (initial) {
            case C1: return A1;
            case G1: return H1;
            case C8: return A8;
            case G8: return H8;
            default: return initial;
        }
    }
}
