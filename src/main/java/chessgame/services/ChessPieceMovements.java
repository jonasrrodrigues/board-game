package chessgame.services;

import board.entities.Position;
import chessgame.entities.enums.ChessPosition;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static chessgame.entities.enums.ChessPosition.toChessPosition;

public class ChessPieceMovements {

    public static Set<ChessPosition> horizontalMovements(ChessPosition position, int moves) {
        Set<ChessPosition> movements = new HashSet<>();
        movements.addAll(horizontalLeftMovements(position, moves));
        movements.addAll(horizontalRightMovements(position, moves));

        return movements;
    }

    public static Set<ChessPosition> horizontalLeftMovements(ChessPosition position, int moves) {
        Set<ChessPosition> movements = new HashSet<>();
        for (int i = 1; i <= moves; i++) {
            movements.add(toChessPosition(new Position(position.row, position.col - i)));
        }

        return movements.stream()
                .filter(move -> move != null)
                .collect(Collectors.toSet());
    }

    public static Set<ChessPosition> horizontalRightMovements(ChessPosition position, int moves) {
        Set<ChessPosition> movements = new HashSet<>();
        for (int i = 1; i <= moves; i++) {
            movements.add(toChessPosition(new Position(position.row, position.col + i)));
        }

        return movements.stream()
                .filter(move -> move != null)
                .collect(Collectors.toSet());
    }

    public static Set<ChessPosition> verticalMovements(ChessPosition position, int moves) {
        Set<ChessPosition> movements = new HashSet<>();
        movements.addAll(verticalUpMovements(position, moves));
        movements.addAll(verticalDownMovements(position, moves));
        return movements;
    }

    public static Set<ChessPosition> verticalUpMovements(ChessPosition position, int moves) {
        Set<ChessPosition> movements = new HashSet<>();
        for (int i = 1; i <= moves; i++) {
            movements.add(toChessPosition(new Position(position.row + i, position.col)));
        }
        return movements.stream()
                .filter(move -> move != null)
                .collect(Collectors.toSet());
    }

    public static Set<ChessPosition> verticalDownMovements(ChessPosition position, int moves) {
        Set<ChessPosition> movements = new HashSet<>();
        for (int i = 1; i <= moves; i++) {
            movements.add(toChessPosition(new Position(position.row - i, position.col)));
        }
        return movements.stream()
                .filter(move -> move != null)
                .collect(Collectors.toSet());
    }

    public static Set<ChessPosition> diagonalMovements(ChessPosition position, int moves) {
        Set<ChessPosition> movements = new HashSet<>();
        movements.addAll(diagonalLeftUpMovements(position, moves));
        movements.addAll(diagonalLeftDownMovements(position, moves));
        movements.addAll(diagonalRightUpMovements(position, moves));
        movements.addAll(diagonalRightDownMovements(position, moves));
        return movements;
    }

    public static Set<ChessPosition> diagonalLeftUpMovements(ChessPosition position, int moves) {
        Set<ChessPosition> movements = new HashSet<>();
        for (int i = 1; i <= moves; i++) {
            movements.add(toChessPosition(new Position(position.row + i, position.col - i)));
        }

        return movements.stream()
                .filter(move -> move != null)
                .collect(Collectors.toSet());
    }

    public static Set<ChessPosition> diagonalLeftDownMovements(ChessPosition position, int moves) {
        Set<ChessPosition> movements = new HashSet<>();
        for (int i = 1; i <= moves; i++) {
            movements.add(toChessPosition(new Position(position.row - i, position.col - i)));
        }

        return movements.stream()
                .filter(move -> move != null)
                .collect(Collectors.toSet());
    }

    public static Set<ChessPosition> diagonalRightUpMovements(ChessPosition position, int moves) {
        Set<ChessPosition> movements = new HashSet<>();
        for (int i = 1; i <= moves; i++) {
            movements.add(toChessPosition(new Position(position.row + i, position.col + i)));
        }

        return movements.stream()
                .filter(move -> move != null)
                .collect(Collectors.toSet());
    }

    public static Set<ChessPosition> diagonalRightDownMovements(ChessPosition position, int moves) {
        Set<ChessPosition> movements = new HashSet<>();
        for (int i = 1; i <= moves; i++) {
            movements.add(toChessPosition(new Position(position.row - i, position.col + i)));
        }

        return movements.stream()
                .filter(move -> move != null)
                .collect(Collectors.toSet());
    }
}
