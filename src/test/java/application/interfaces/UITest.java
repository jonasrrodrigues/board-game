package application.interfaces;

import board.entities.enums.PieceColor;
import board.entities.exceptions.BoardException;
import chessgame.entities.enums.ChessPosition;
import chessgame.entities.exeptions.ChessGameServiceException;
import chessgame.entities.pieces.Bishop;
import chessgame.entities.pieces.Pawn;
import chessgame.entities.pieces.Queen;
import chessgame.services.ChessGameService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static chessgame.entities.enums.ChessPosition.*;

class UITest {

    private Queen queen;
    @BeforeEach

    void setUp() {
        queen = new Queen(PieceColor.BLACK);
    }

    @Test
    void printBoardTest() {
        ChessGameService service =  new ChessGameService();
        System.out.println(UI.printBoardState(service.getBoard()));
    }

    @Test
    void printPossibleMoves() throws BoardException, ChessGameServiceException {
        ChessGameService service =  new ChessGameService();
        service.getBoard().removePiece(B1);
        service.getBoard().removePiece(A1);
        service.getBoard().removePiece(F1);
        service.getBoard().removePiece(C1);
        service.getBoard().removePiece(G1);
        service.getBoard().removePiece(G7);
        service.getBoard().removePiece(D1);
        service.getBoard().placePiece(new Queen(PieceColor.WITHE), D4);
        service.getBoard().placePiece(new Bishop(PieceColor.WITHE), B4);
        service.getBoard().placePiece(new Pawn(PieceColor.WITHE), F6);

        Set<ChessPosition> chessPositions = service.possibleMovesInTheBoard(service.getChessPiece(D4), D4);
        System.out.print(UI.printPossibleMoves(service.getBoard(), chessPositions));
    }
}