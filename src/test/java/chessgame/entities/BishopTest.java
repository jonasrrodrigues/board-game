package chessgame.entities;

import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;
import chessgame.entities.pieces.Bishop;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static chessgame.entities.enums.ChessPosition.C1;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BishopTest {

    @Test
    void moveRules() {
        Bishop bishop = new Bishop(PieceColor.BLACK);
        Set<ChessPosition> possiblePositions = bishop.possibleMoves(C1);

        assertTrue(possiblePositions.size() == 7);
        assertTrue(possiblePositions.contains(ChessPosition.B2));
        assertTrue(possiblePositions.contains(ChessPosition.A3));
        assertTrue(possiblePositions.contains(ChessPosition.D2));
        assertTrue(possiblePositions.contains(ChessPosition.E3));

    }
}