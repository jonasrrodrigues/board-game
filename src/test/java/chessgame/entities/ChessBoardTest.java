package chessgame.entities;

import board.entities.exceptions.BoardException;
import chessgame.entities.board.ChessBoard;
import chessgame.entities.enums.ChessPosition;
import chessgame.entities.pieces.ChessPiece;
import chessgame.entities.pieces.King;
import chessgame.entities.pieces.Pawn;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static board.entities.enums.PieceColor.BLACK;
import static board.entities.enums.PieceColor.WITHE;
import static chessgame.entities.enums.ChessPosition.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class ChessBoardTest {

    private ChessBoard chessBoard;
    private ChessPiece piece;

    @BeforeEach
    void setUp() throws BoardException {
        chessBoard = new ChessBoard();
        piece = mock(Pawn.class);
    }

    @Test
    void getPiece() throws BoardException {
        chessBoard.placePiece(piece, A2);
        assertTrue(chessBoard.getChessPiece(A2) != null);
    }

    @Test
    void getPieceNull() throws BoardException {
        assertTrue(chessBoard.getChessPiece(null) == null);
    }


    @Test
    void placePiece() throws BoardException {
        chessBoard.placePiece(piece, A2);
        assertEquals(piece, chessBoard.getChessPiece(A2));
    }

    @Test
    void havePiece() throws BoardException {
        chessBoard.placePiece(piece, A2);
        assertTrue(chessBoard.havePiece(A2));
        assertFalse(chessBoard.havePiece(A1));
    }

    @Test
    void getAllPieces() throws BoardException {
        chessBoard.placePiece(new King(WITHE), E1);
        chessBoard.placePiece(new King(BLACK), E8);
        Map<ChessPiece, ChessPosition> allPieces = chessBoard.getAllPieces();
        assertEquals(2, allPieces.size());
    }
}