package chessgame.entities;

import board.entities.enums.PieceColor;
import chessgame.entities.board.ChessGame;
import chessgame.entities.pieces.ChessPiece;
import chessgame.entities.pieces.Pawn;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class ChessGameTest {

    private ChessGame game;
    private ChessPiece piece;

    @BeforeEach
    void setUp() {
        piece = Mockito.mock(Pawn.class);
        game =  new ChessGame();
        when(piece.getColor()).thenReturn(PieceColor.BLACK);
    }

    @Test
    void nextRoud() {
        game.nextRound();
        assertEquals(1, game.getRound());
    }

    @Test
    void nextTurn() {
        assertEquals(PieceColor.WITHE, game.getTurnOf());
        game.nextRound();
        assertEquals(PieceColor.BLACK, game.getTurnOf());
    }

    @Test
    void capturePiece() {
        game.capturePiece(piece);
        assertEquals(1, game.getBlackDeads().size());
    }
}