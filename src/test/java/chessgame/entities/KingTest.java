package chessgame.entities;

import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;
import chessgame.entities.pieces.King;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static chessgame.entities.enums.ChessPosition.E1;
import static org.junit.jupiter.api.Assertions.assertTrue;

class KingTest {

    @Test
    void moveRules() {
        King king = new King(PieceColor.WITHE);
        king.setFirstMove();
        Set<ChessPosition> possiblePositions = king.possibleMoves(E1);

        assertTrue(possiblePositions.size() == 5);
        assertTrue(possiblePositions.contains(ChessPosition.D2));
    }
}