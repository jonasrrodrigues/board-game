package chessgame.entities;

import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;
import chessgame.entities.pieces.Knight;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static chessgame.entities.enums.ChessPosition.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

class KnightTest {

    private Knight knight;

    @BeforeEach
    void setUp() {
        knight =  new Knight(PieceColor.WITHE);
    }

    @Test
    void possibleMoves() {
        Set<ChessPosition> possibleMoves = knight.possibleMoves(C3);
        assertTrue(possibleMoves.contains(B1), "Don't move to B1");
        assertTrue(possibleMoves.contains(D1), "Don't move to D1");
        assertTrue(possibleMoves.contains(A2), "Don't move to A2");
        assertTrue(possibleMoves.contains(E2), "Don't move to E2");
        assertTrue(possibleMoves.contains(A4), "Don't move to A4");
        assertTrue(possibleMoves.contains(E4), "Don't move to E4");
        assertTrue(possibleMoves.contains(B5), "Don't move to B5");
        assertTrue(possibleMoves.contains(D5), "Don't move to D5");
    }

    @Test
    public void equals() {
        assertTrue(new Knight(PieceColor.WITHE).equals(knight));
    }
}