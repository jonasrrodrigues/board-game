package chessgame.entities;

import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;
import chessgame.entities.pieces.Pawn;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static chessgame.entities.enums.ChessPosition.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PawnTest {

    private Pawn pawn;

    @BeforeEach
    void setUp() {
        pawn =  new Pawn(PieceColor.WITHE);
    }

    @Test
    public void possibleSecondMoves() {
        pawn.setFirstMove();
        Set<ChessPosition> chessPositions = pawn.possibleMoves(A2);
        assertTrue(chessPositions.contains(A3), "Don't move to A3");
        assertFalse(chessPositions.contains(A4), "move to A4 is possible after first move");
    }

    @Test
    public void forwardMove() {
        Set<ChessPosition> chessPositions = pawn.possibleMoves(A2);
        assertTrue(chessPositions.contains(A3), "Don't move to A3");
        assertTrue(chessPositions.contains(A4), "Don't move to A4");
    }

    @Test
    public void blackForwardMove() {
        pawn = new Pawn(PieceColor.BLACK);
        Set<ChessPosition> chessPositions = pawn.possibleMoves(A7);
        assertTrue(chessPositions.contains(A6), "Don't move to A6");
        assertTrue(chessPositions.contains(A5), "Don't move to A5");
    }
}