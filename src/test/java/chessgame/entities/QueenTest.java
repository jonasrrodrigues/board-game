package chessgame.entities;

import board.entities.enums.PieceColor;
import chessgame.entities.board.ChessBoard;
import chessgame.entities.pieces.Queen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static chessgame.entities.enums.ChessPosition.D1;
import static chessgame.entities.enums.ChessPosition.D2;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class QueenTest {

    private ChessBoard board;

    @BeforeEach
    void setUp() {
    }

    @Test
    void moveRules() {
        Queen queen =  new Queen(PieceColor.BLACK);
        assertEquals(21, queen.possibleMoves(D1).size());
        assertTrue(queen.possibleMoves(D1).contains(D2));
    }
}