package chessgame.entities;

import board.entities.enums.PieceColor;
import chessgame.entities.enums.ChessPosition;
import chessgame.entities.pieces.Rook;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static chessgame.entities.enums.ChessPosition.A1;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RookTest {

    @Test
    void possibleMoves() {
        Rook rook = new Rook(PieceColor.WITHE);
        rook.setFirstMove();
        Set<ChessPosition> possiblePositions = rook.possibleMoves(A1);

        assertTrue(possiblePositions.size() == 14);
        assertTrue(possiblePositions.contains(ChessPosition.A7));
        assertTrue(possiblePositions.contains(ChessPosition.H1));
    }
}