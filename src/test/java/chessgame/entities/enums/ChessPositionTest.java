package chessgame.entities.enums;

import board.entities.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ChessPositionTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    public void stringOfPosition() {
        assertEquals("A1", ChessPosition.A1.toString());
    }

    @Test
    public void toChessPositionTest() {
        final Position position = new Position(0, 0);
        assertEquals(ChessPosition.A1, ChessPosition.toChessPosition(position));
    }
}