package chessgame.services;

import application.interfaces.UI;
import board.entities.enums.PieceColor;
import board.entities.exceptions.BoardException;
import chessgame.entities.board.ChessBoard;
import chessgame.entities.board.ChessGame;
import chessgame.entities.enums.ChessPosition;
import chessgame.entities.exeptions.ChessGameServiceException;
import chessgame.entities.pieces.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static board.entities.enums.PieceColor.BLACK;
import static board.entities.enums.PieceColor.WITHE;
import static chessgame.entities.enums.ChessPosition.*;
import static org.junit.jupiter.api.Assertions.*;

class ChessGameServiceTest {

    private ChessGameService service;
    private ChessBoard board;
    private ChessPiece enemyPiece;
    private ChessPiece friendlyPiece;

    @BeforeEach
    void setUp() throws BoardException {
        service =  new ChessGameService();
        board =  new ChessBoard();
    }

    @Test
    void startGame() {
        board = service.getBoard();

        assertTrue(board.getChessPiece(A2) instanceof Pawn, "White Pawn not in A2");
        assertTrue(board.getChessPiece(B2) instanceof Pawn, "White Pawn not in B2");
        assertTrue(board.getChessPiece(C2) instanceof Pawn, "White Pawn not in C2");
        assertTrue(board.getChessPiece(D2) instanceof Pawn, "White Pawn not in D2");
        assertTrue(board.getChessPiece(E2) instanceof Pawn, "White Pawn not in E2");
        assertTrue(board.getChessPiece(F2) instanceof Pawn, "White Pawn not in F2");
        assertTrue(board.getChessPiece(G2) instanceof Pawn, "White Pawn not in G2");
        assertTrue(board.getChessPiece(H2) instanceof Pawn, "White Pawn not in H2");

        assertTrue(board.getChessPiece(A7) instanceof Pawn, "Black Pawn not in A7");
        assertTrue(board.getChessPiece(B7) instanceof Pawn, "Black Pawn not in B7");
        assertTrue(board.getChessPiece(C7) instanceof Pawn, "Black Pawn not in C7");
        assertTrue(board.getChessPiece(D7) instanceof Pawn, "Black Pawn not in D7");
        assertTrue(board.getChessPiece(E7) instanceof Pawn, "Black Pawn not in E7");
        assertTrue(board.getChessPiece(F7) instanceof Pawn, "Black Pawn not in F7");
        assertTrue(board.getChessPiece(G7) instanceof Pawn, "Black Pawn not in G7");
        assertTrue(board.getChessPiece(H7) instanceof Pawn, "Black Pawn not in H7");

        assertTrue(board.getChessPiece(C1) instanceof Bishop, "Withe bishop not in C1");
        assertTrue(board.getChessPiece(F1) instanceof Bishop, "Withe bishop not in F1");
        assertTrue(board.getChessPiece(C8) instanceof Bishop, "Withe bishop not in C8");
        assertTrue(board.getChessPiece(F8) instanceof Bishop, "Withe bishop not in F8");

        assertTrue(board.getChessPiece(B1) instanceof Knight, "Withe Knight not in B1");
        assertTrue(board.getChessPiece(G1) instanceof Knight, "Withe Knight not in G1");
        assertTrue(board.getChessPiece(B8) instanceof Knight, "Withe Knight not in B8");
        assertTrue(board.getChessPiece(G8) instanceof Knight, "Withe Knight not in G8");

        assertTrue(board.getChessPiece(A1) instanceof Rook, "Withe Rook not in A1");
        assertTrue(board.getChessPiece(H1) instanceof Rook, "Withe Rook not in H1");
        assertTrue(board.getChessPiece(A8) instanceof Rook, "Withe Rook not in A8");
        assertTrue(board.getChessPiece(H8) instanceof Rook, "Withe Rook not in H8");

        assertTrue(board.getChessPiece(D1) instanceof Queen, "Withe queen not in D1");
        assertTrue(board.getChessPiece(D8) instanceof Queen, "Withe queen not in D8");

        assertTrue(board.getChessPiece(E1) instanceof King, "Withe king not in E1");
        assertTrue(board.getChessPiece(E8) instanceof King, "Withe king not in E8");

    }

    @Test
    void movePawnPiece() throws ChessGameServiceException {
        service.movePiece(A2, A4);
        assertTrue(service.getChessPiece(A4) != null, "Piece is not on A4!");

        Pawn pawnInNewPosition = (Pawn) service.getChessPiece(A4);

        assertFalse(pawnInNewPosition.isFirstMove());
    }

    @Test
    void capturePiece() throws BoardException, ChessGameServiceException {
        board = new ChessBoard();
        enemyPiece = new Pawn(BLACK);
        friendlyPiece = new Bishop(WITHE);
        board.placePiece(enemyPiece, E3);
        board.placePiece(friendlyPiece, C1);
        service = new ChessGameService(board, new ChessGame());
        service.movePiece(C1, E3);
        assertEquals(friendlyPiece, service.getChessPiece(E3));
        assertEquals(Collections.singletonList(enemyPiece), service.getCapturedOf(BLACK));
    }

    @Test
    void unableCaptureYourPiece() throws BoardException, ChessGameServiceException {
        board = new ChessBoard();
        enemyPiece = new Pawn(WITHE);
        friendlyPiece = new Bishop(WITHE);
        board.placePiece(enemyPiece, E3);
        board.placePiece(friendlyPiece, C1);
        service = new ChessGameService(board, new ChessGame());

        try {
            service.movePiece(C1, E3);
        } catch (ChessGameServiceException e) {
            assertEquals("Don't possible place the piece B in position E3", e.getMessage());
        }
    }

    @Test
    void invalidPiece() {
        try {
            service.movePiece(A7, A6);
        } catch (ChessGameServiceException e) {
            assertEquals("Unable to move adversary piece", e.getMessage());
        }
    }

    @Test
    void noMovePositions()  {
        board = service.getBoard(); // chess starts with whites pieces
        try {
            service.movePiece(E1, E2);
        } catch (ChessGameServiceException e) {
            assertEquals("Has no possible moves for this piece!", e.getMessage());
        }
    }

    @Test
    void nextTurn() throws ChessGameServiceException {
        assertEquals(WITHE, service.getTurn());
        service.movePiece(A2, A4);
        assertEquals(BLACK, service.getTurn());
        assertEquals(1, service.getRound());
    }

    @Test
    void possibleMovesOfQueen() throws BoardException {
        List<ChessPosition> expectedPositions = Arrays.asList(A7, B6, C5, C4, C3, D7, D6, D5, D3, E5, E4, E3, F4, G4, H4);
        service.getBoard().removePiece(B1);
        service.getBoard().removePiece(A1);
        service.getBoard().removePiece(F1);
        service.getBoard().removePiece(C1);
        service.getBoard().removePiece(G1);
        service.getBoard().removePiece(G7);
        service.getBoard().removePiece(D1);
        service.getBoard().placePiece(new Queen(PieceColor.WITHE), D4);
        service.getBoard().placePiece(new Bishop(PieceColor.WITHE), B4);
        service.getBoard().placePiece(new Pawn(PieceColor.WITHE), F6);

        Set<ChessPosition> chessPositions = service.possibleMovesInTheBoard(service.getChessPiece(D4), D4);
        assertTrue(chessPositions.containsAll(expectedPositions));
        assertEquals(expectedPositions.size(), chessPositions.size());
    }

    @Test
    void possibleMovesOfPawn() throws BoardException {
        List<ChessPosition> expectedPositions = Arrays.asList(E5, D5);
        enemyPiece = new Pawn(BLACK);
        friendlyPiece = new Pawn(WITHE);
        friendlyPiece.setFirstMove();
        service.getBoard().placePiece(friendlyPiece, E4);
        service.getBoard().placePiece(enemyPiece, D5);

        Set<ChessPosition> chessPositions = service.possibleMovesInTheBoard(service.getChessPiece(E4), E4);
        assertTrue(chessPositions.containsAll(expectedPositions));
        assertEquals(expectedPositions.size(), chessPositions.size());
    }

    @Test
    void gameSimulateBlackWins() throws ChessGameServiceException {
        service.movePiece(F2, F3);
        service.movePiece(E7, E6);
        service.movePiece(G2, G4);
        service.movePiece(D8, H4);
        assertEquals(4, service.getRound());
        assertTrue(service.isCheckmate());
        assertEquals(WITHE, service.getTurn());
    }

    @Test
    void getCheckWithCapture() throws ChessGameServiceException {
        service.getBoard().removePiece(H2);
        service.movePiece(F2, F3);
        service.movePiece(E7, E6);
        service.movePiece(G2, G4);
        service.movePiece(D8, H4);
        assertEquals(4, service.getRound());
        assertFalse(service.isCheckmate());
        assertEquals(WITHE, service.getTurn());
    }

    @Test
    void simulateCheckInWhite() throws ChessGameServiceException {
        service.movePiece(E2, E4);
        service.movePiece(D7, D5);
        service.movePiece(E4, D5);
        service.movePiece(D8, D5);
        service.movePiece(C2, C3);
        service.movePiece(D5, E5);

        System.out.print(UI.printBoardState(service.getBoard()));
        assertEquals(6, service.getRound());
        assertTrue(service.isKingInCheck());
        assertFalse(service.isCheckmate());
        assertEquals(WITHE, service.getTurn());
    }

    @Test
    void movesKingToCheckPosition() throws ChessGameServiceException {
        service.movePiece(A2, A3);
        service.movePiece(E7, E6);
        service.movePiece(A3, A4);
        service.movePiece(D8, H4);
        try {
            service.movePiece(F2, F3);
        } catch (ChessGameServiceException e) {
            assertEquals("Can't move to a position that leaves the king in check.", e.getMessage());
        }
    }

    @Test
    void whiteLongCastling() throws ChessGameServiceException {
        service.getBoard().removePiece(B1);
        service.getBoard().removePiece(C1);
        service.getBoard().removePiece(D1);
        service.movePiece(E1, C1);

        assertEquals(new King(WITHE), service.getChessPiece(C1));
        assertEquals( new Rook(WITHE), service.getChessPiece(D1));
    }

    @Test
    void whiteShortCastling() throws ChessGameServiceException {
        board = service.getBoard();
        board.removePiece(F1);
        board.removePiece(G1);
        service.movePiece(E1, G1);

        assertEquals(new King(WITHE), service.getChessPiece(G1));
        assertEquals( new Rook(WITHE), service.getChessPiece(F1));
        assertEquals( null, service.getChessPiece(H1));
    }

    @Test
    void impossibleWhiteLongCastling() throws ChessGameServiceException, BoardException {
        board = service.getBoard();
        board.removePiece(B1);
        board.removePiece(C1);
        board.removePiece(D1);
        board.removePiece(E2);
        board.placePiece(new Queen(BLACK), H5);
        try {
            service.movePiece(E1, C1);
        } catch (ChessGameServiceException e) {
            assertEquals("Don't possible place the piece K in position C1", e.getMessage());
        }
    }

    @Test
    void promotionWhitePawn() throws ChessGameServiceException, BoardException {
        board = service.getBoard();
        board.removePiece(A7);
        board.removePiece(A8);
        board.placePiece(new Pawn(WITHE), A7);
        service.movePiece(A7, A8);
        assertEquals(new Queen(WITHE), service.getChessPiece(A8));
    }

    @Test
    void promotionBlackPawn() throws ChessGameServiceException, BoardException {
        board = service.getBoard();
        board.removePiece(A1);
        board.removePiece(A2);
        board.placePiece(new Pawn(BLACK), A2);
        service.movePiece(B2, B3);
        service.movePiece(A2, A1);
        assertEquals(new Queen(BLACK), service.getChessPiece(A1));
    }

    @Test
    void enPassantInA4() throws BoardException, ChessGameServiceException {
        board = service.getBoard();
        ChessPiece blackPawn = new Pawn(BLACK);
        board.placePiece(blackPawn, B4);
        service.movePiece(A2, A4);
        service.movePiece(B4, A3);
        assertEquals(blackPawn, service.getChessPiece(A3));
    }

    @Test
    void enPassantInB5() throws BoardException, ChessGameServiceException {
        board = service.getBoard();
        ChessPiece whitePawn = new Pawn(WITHE);
        board.placePiece(whitePawn, A4);
        service.movePiece(A4, A5);
        service.movePiece(B7, B5);
        service.movePiece(A5, B6);
        assertEquals(whitePawn, service.getChessPiece(B6));
    }
}